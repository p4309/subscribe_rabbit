# frozen_string_literal: true

RSpec.describe SubscribeRabbit do
  it 'has a version number' do
    expect(SubscribeRabbit::VERSION).not_to be nil
  end
end
