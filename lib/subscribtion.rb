# frozen_string_literal: true
require 'active_support'
require 'active_support/core_ext'
require 'bunny'

class Subscription
  module Env
    def self.development?
      env_by_name(:development)
    end

    def self.test?
      env_by_name(:test)
    end

    def self.env_by_name(name)
      if defined?(Rails)
        Rails.env.send("#{name}?")
      else
        ENV.fetch(name.to_s.upcase, false).present?
      end
    end
  end

  class Connector
    BUNNY_HOST = (Env.development? ? '127.0.0.1' : 'rabbitmq').freeze
    attr_reader :channel, :conn

    def initialize(host = BUNNY_HOST)
      return if Env.test?

      connect(host)
    end

    def connect(host = BUNNY_HOST)
      failure_counter = 0
      @conn = Bunny.new("amqp://guest:guest@#{host}:5672")
      @conn.start
      @channel = @conn.create_channel
    rescue Bunny::TCPConnectionFailedForAllHosts => _e
      failure_counter += 1
      raise 'Too many network errors' if failure_counter > 100

      sleep 1
      retry
    rescue Bunny::NetworkFailure => _e
      failure_counter += 1
      raise 'Too many network errors' if failure_counter > 100

      ch.maybe_kill_consumer_work_pool!
      sleep 2
      retry
    end
  end

  class QueuePool
    def initialize
      @connector = ::Subscription::Connector.new
      @queues = {}
    end

    def get_queue(queue_name)
      return if queue_name.to_s == ''

      @queues[queue_name.to_sym] ||= create_queue(queue_name)
    end

    private

    def create_queue(queue_name)
      @connector.channel.queue(queue_name.to_s)
    end
  end

  Mapping = Struct.new(:in_queue_name, :out_queue_name, :method_name, :subscription_class)

  @@mappings = [] # rubocop:disable Style/ClassVars
  class << self
    def mappings
      @@mappings
    end

    def subscribe(in_queue_name, out_queue_name = nil, method_name = :work)
      mappings << Mapping.new(in_queue_name, out_queue_name, method_name, self)
    end

    def start_subscriptions
      mappings.each do |mapping|
        instance = mapping.subscription_class.new
        instance.subscribe_and_publish(mapping.in_queue_name, mapping.out_queue_name) do |message|
          instance.send(mapping.method_name, message)
        end
      end
    end
  end

  def publish(publish_channel, message)
    queue_pool = Subscription::QueuePool.new
    queue_pool.get_queue(publish_channel)&.publish(message.to_yaml)
  end

  def subscribe_and_publish(subscribe_channel, publish_channel)
    queue_pool = Subscription::QueuePool.new
    message_input_queue = queue_pool.get_queue(subscribe_channel)

    message_input_queue.subscribe do |_delivery_info, _metadata, payload|
      message = InternalMessage.from_yaml(payload)
      output_message = yield(message)
      queue_pool.get_queue(publish_channel)&.publish(output_message.to_yaml) if output_message.present? && publish_channel.present?
    end
  rescue Bunny::TCPConnectionFailedForAllHosts => _e
    failure_counter += 1
    if failure_counter > 50
      logger.fatal 'Too many network errors'
      raise 'Too many network errors'
    end
    logger.warn 'Bunny::TCPConnectionFailedForAllHosts'
    sleep 1
    retry
  rescue Bunny::NetworkFailure => _e
    failure_counter += 1
    if failure_counter > 50
      logger.fatal 'Too many network errors'
      raise 'Too many network errors'
    end
    logger.warn 'Bunny::NetworkFailure'
    sleep 1
    retry
  end
end
